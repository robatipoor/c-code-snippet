#!/usr/bin/env bash

gcc examples/$1.c -o $1 # compile example code
./$1 $2 # run program
rm $1 # clean directory


